import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Widget _chatCard() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 80,
      color: Colors.white,
      padding: EdgeInsets.all(2),
      margin: EdgeInsets.only(bottom: 5),
      child: Row(
        children: [
          Container(
            width: 75,
            height: 75,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        'https://t3.ftcdn.net/jpg/01/24/90/12/240_F_124901236_P93UTdvjcstjvhjQmbtpc63sfpL238TI.jpg'),
                    fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(75),
                color: Colors.teal),
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Ahmed El masry',
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10,
              ),
              Text('How Are You ?')
            ],
          ),
          Expanded(child: SizedBox()),
          Text('12:30 pm')
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: Text('Home Page'),
          backgroundColor: Colors.deepOrangeAccent,
          leading: IconButton(
              icon: Icon(
                Icons.notifications,
                size: 30,
                color: Colors.white,
              ),
              onPressed: null),
        ),
        body: ListView.builder(
          itemCount: 100,
          itemBuilder: (context, i) {
            return _chatCard();
          },
        ));
  }
}
