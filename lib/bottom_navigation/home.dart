import 'package:flutter/material.dart';
import 'package:group5/bottom_navigation/dm.dart';
import 'package:group5/bottom_navigation/notification.dart';
import 'package:group5/bottom_navigation/search.dart';
import 'package:group5/bottom_navigation/time.dart';

class BottomNavigationScreen extends StatefulWidget {
  @override
  _BottomNavigationScreenState createState() => _BottomNavigationScreenState();
}

class _BottomNavigationScreenState extends State<BottomNavigationScreen> {
  var _screens = [
    TimeScreen(),
    SearchScreen(),
    NotificationScreen(),
    DmScreen()
  ];
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: _currentIndex,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  size: 30,
                ),
                label: ''),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.search,
                  size: 30,
                ),
                label: ''),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.notifications,
                  size: 30,
                ),
                label: ''),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.mail_outline,
                  size: 30,
                ),
                label: ''),
          ],
        ),
        body: _screens[_currentIndex]);
  }
}
