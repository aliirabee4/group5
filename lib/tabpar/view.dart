import 'package:flutter/material.dart';
import 'package:group5/tabpar/calls.dart';
import 'package:group5/tabpar/camera.dart';
import 'package:group5/tabpar/chats.dart';
import 'package:group5/tabpar/status.dart';

class TabBarScreen extends StatefulWidget {
  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  @override
  Widget build(BuildContext context) {
    _tabController = TabController(length: 4, vsync: this);
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          'WhatsApp',
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.green,
        bottom: TabBar(
          controller: _tabController,
          tabs: [
            Icon(Icons.camera_alt),
            Tab(
              text: 'Chats',
            ),
            Tab(
              text: 'Status',
            ),
            Tab(
              text: 'Calls',
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          CameraScreen(),
          ChatsScreen(),
          StatusScreen(),
          CallsScreen(),
        ],
      ),
    );
  }
}
