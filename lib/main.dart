import 'package:flutter/material.dart';
import 'package:group5/map/map.dart';
// import 'package:group5/packages/home.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
// import 'package:group5/basket.dart';
// import 'package:group5/tawla/home.dart';
// import 'package:group5/tabpar/view.dart';
// import 'package:group5/restaurant/home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await translator.init(
    localeDefault: LocalizationDefaultType.device,
    languagesList: <String>['ar', 'en'],
    assetsDirectory: 'assets/langs/',
  );
  runApp(LocalizedApp(child: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MapScreen(),
      theme: ThemeData(fontFamily: 'Tajawal'),
      localizationsDelegates: translator.delegates,
      locale: translator.locale,
      supportedLocales: translator.locals(),
    );
  }
}

// class HomeSceen extends StatefulWidget {
//   @override
//   _HomeSceenState createState() => _HomeSceenState();
// }

// class _HomeSceenState extends State<HomeSceen> {
//   // Widget _buildContainer(String title) {
//   //   return Container(
//   //     width: 100,
//   //     height: 100,
//   //     margin: EdgeInsets.all(10),
//   //     decoration: BoxDecoration(
//   //         color: Colors.teal, borderRadius: BorderRadius.circular(5)),
//   //     child: Center(
//   //       child: Text(
//   //         title,
//   //         style: TextStyle(color: Colors.white, fontSize: 20),
//   //       ),
//   //     ),
//   //   );
//   // }

//   Widget _textInput(String hint, bool secure, Icon icon) {
//     return Padding(
//       padding: EdgeInsets.all(30),
//       child: TextFormField(
//         obscureText: secure,
//         decoration: InputDecoration(
//             suffixIcon: icon,
//             enabledBorder:
//                 OutlineInputBorder(borderSide: BorderSide(color: Colors.teal)),
//             focusedBorder:
//                 OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
//             border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
//             hintText: hint,
//             hintStyle: TextStyle(color: Colors.black)),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         backgroundColor: Colors.teal,
//         centerTitle: true,
//         title: Text(
//           'Login Screen',
//           style: TextStyle(
//               color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
//         ),
//         leading: Icon(Icons.person),
//         actions: [Icon(Icons.more_vert)],
//       ),
//       body: Column(
//         children: [
//           SizedBox(
//             height: 150,
//           ),
//           _textInput(
//               'User Name',
//               false,
//               Icon(
//                 Icons.person,
//                 color: Colors.black,
//               )),
//           _textInput(
//               'Password',
//               true,
//               Icon(
//                 Icons.remove_red_eye,
//                 color: Colors.black,
//               )),
//           Container(
//             width: MediaQuery.of(context).size.width,
//             height: 60,
//             margin: EdgeInsets.only(left: 30, right: 30),
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(10), color: Colors.teal),
//             child: Center(
//               child: Text(
//                 'Login',
//                 style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 20,
//                     fontWeight: FontWeight.bold),
//               ),
//             ),
//           ),
//           SizedBox(
//             height: 20,
//           ),
//           Text('Dont Have an Account ? ')
//         ],
//       ),
//     );
//   }
// }
