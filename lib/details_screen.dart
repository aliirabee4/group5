import 'package:flutter/material.dart';

class DetailsScreen extends StatefulWidget {
  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Details',
          style: TextStyle(color: Colors.grey),
        ),
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: null),
        actions: [
          IconButton(
              icon: Icon(
                Icons.favorite,
              ),
              onPressed: null)
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 2,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        'https://t4.ftcdn.net/jpg/02/68/17/95/240_F_268179518_JxhMIr78eZna4MNeI7XJGme4w8kneZon.jpg'))),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              'Apple Tree',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 10),
            child: Text(
              'Price 1200 LE',
              style: TextStyle(fontSize: 14, color: Colors.grey),
            ),
          )
        ],
      ),
    );
  }
}
