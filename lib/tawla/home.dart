import 'package:flutter/material.dart';

class TawlaHomeScreen extends StatefulWidget {
  @override
  _TawlaHomeScreenState createState() => _TawlaHomeScreenState();
}

class _TawlaHomeScreenState extends State<TawlaHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            actions: [
              Container(
                margin: EdgeInsets.all(5),
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/appIcon.png'),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(40),
                    color: Colors.white),
              )
            ],
            leading: Padding(
              padding: EdgeInsets.all(12),
              child: Image.asset(
                'assets/images/notification.png',
                width: 20,
                height: 20,
              ),
            )),
        body: Center(
            child: Text(
          'اهلا',
          style: TextStyle(
            fontSize: 20,
          ),
        )));
  }
}
